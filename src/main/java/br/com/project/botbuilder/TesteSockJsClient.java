package br.com.project.botbuilder;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import org.springframework.messaging.converter.StringMessageConverter;
import org.springframework.messaging.simp.stomp.StompFrameHandler;
import org.springframework.messaging.simp.stomp.StompHeaders;
import org.springframework.messaging.simp.stomp.StompSession;
import org.springframework.messaging.simp.stomp.StompSessionHandlerAdapter;
import org.springframework.web.socket.client.standard.StandardWebSocketClient;
import org.springframework.web.socket.messaging.WebSocketStompClient;
import org.springframework.web.socket.sockjs.client.RestTemplateXhrTransport;
import org.springframework.web.socket.sockjs.client.SockJsClient;
import org.springframework.web.socket.sockjs.client.Transport;
import org.springframework.web.socket.sockjs.client.WebSocketTransport;

public class TesteSockJsClient {

	public static void main(String[] args) {
		
		List<Transport> transports = new ArrayList<>(2);
		transports.add(new WebSocketTransport(new StandardWebSocketClient()));
		transports.add(new RestTemplateXhrTransport());
		SockJsClient sockJsClient = new SockJsClient(transports);
		
		String url = "http://localhost:8282/botbuilder/websocketgame";
		
		WebSocketStompClient stompClient = new WebSocketStompClient(sockJsClient);
//		stompClient.setMessageConverter(new StringMessageConverter());
		stompClient.connect(url, new StompSessionHandlerAdapter() {
			
			@Override
			public Type getPayloadType(StompHeaders headers) {
				//return super.getPayloadType(headers);
				return String.class;
			}
			
			@Override
			public void handleFrame(StompHeaders headers, Object payload) {
				System.out.println("handlerFrame STOMPClient");
			}
			
			@Override
			public void afterConnected(StompSession session, StompHeaders connectedHeaders) {
				
				StompFrameHandler handler = new StompFrameHandler() {
		            @Override
		            public Type getPayloadType(StompHeaders stompHeaders) {
		            	System.out.println("received ");
		                return String.class;
		            }

		            @Override
		            public void handleFrame(StompHeaders stompHeaders, Object o) {
		            	String t = (String) o;
		            	System.out.println(t);
		                System.out.println("received " + o.toString());
		            }
		        };

		        StompSession.Subscription s = session.subscribe("/app/playerid", handler);
//				session.subscribe("/app/playerid", new StompSessionHandlerAdapter(){
//
//					@Override
//					public Type getPayloadType(StompHeaders headers) {
//						System.out.println("getPayloadType playerid");
//						return String.class;
//					}
//
//					@Override
//					public void handleFrame(StompHeaders headers, Object payload) {
//						System.out.println("handleFrame playerid");
//					}
//					
//					
//					
//				});
				
				session.subscribe("/topic/playerid", new StompSessionHandlerAdapter(){

					@Override
					public Type getPayloadType(StompHeaders headers) {
						System.out.println("getPayloadType playerid");
						return String.class;
					}

					@Override
					public void handleFrame(StompHeaders headers, Object payload) {
						System.out.println("handleFrame playerid");
					}
					
				});
				
				session.subscribe("/app/playersconnected", new StompFrameHandler(){

					@Override
					public Type getPayloadType(StompHeaders headers) {
						System.out.println("getPayloadType playersconnected");
						return String.class;
					}

					@Override
					public void handleFrame(StompHeaders headers, Object payload) {
						System.out.println("handleFrame playersconnected");
					}
					
				});
				
				session.subscribe("/topic/newplayer", new StompFrameHandler(){

					@Override
					public Type getPayloadType(StompHeaders headers) {
						System.out.println("getPayloadType newplayer");
						return String.class;
					}

					@Override
					public void handleFrame(StompHeaders headers, Object payload) {
						System.out.println("handleFrame newplayer");
					}
				});
				
				session.subscribe("/topic/playermoved", new StompFrameHandler(){

					@Override
					public Type getPayloadType(StompHeaders headers) {
						System.out.println("getPayloadType playermoved");
						return String.class;
					}

					@Override
					public void handleFrame(StompHeaders headers, Object payload) {
						System.out.println("handleFrame playermoved");
					}
				});
				
				session.subscribe("/topic/playerdisconnect", new StompFrameHandler(){

					@Override
					public Type getPayloadType(StompHeaders headers) {
						System.out.println("getPayloadType playerdisconnect");
						return String.class;
					}

					@Override
					public void handleFrame(StompHeaders headers, Object payload) {
						System.out.println("handleFrame playerdisconnect");
					}
				});
				
				/*service.subscribe("/topic/newplayer", function(data){
					console.log(data.body);
				});
				
				service.subscribe("/topic/playermoved", function(data){
					console.log(data.body);
				});
				
				service.subscribe("/app/playerid", function(data){
					console.log(data.body);
				});
				
				service.subscribe("/app/playersconnected", function(data){
					console.log(data.body);
				});
				
				service.subscribe("/topic/playerdisconnect", function(data){
					console.log(data.body);
				});*/
			}
		});
		
		stompClient.start();
		
		while(true);
	}

}

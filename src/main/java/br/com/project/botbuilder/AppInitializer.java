package br.com.project.botbuilder;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletRegistration.Dynamic;

import org.springframework.web.WebApplicationInitializer;
import org.springframework.web.context.ContextLoaderListener;
import org.springframework.web.context.support.AnnotationConfigWebApplicationContext;
import org.springframework.web.servlet.DispatcherServlet;

public class AppInitializer implements WebApplicationInitializer {

	@Override
	public void onStartup(ServletContext container) throws ServletException {
		AnnotationConfigWebApplicationContext rootContext;
		rootContext = buildApplicationContext("br.com.project.botbuilder.configuration");
		container.addListener(new ContextLoaderListener(rootContext));
		
		Dynamic appServlet = container.addServlet("appServlet", new DispatcherServlet(rootContext));
		appServlet.addMapping("/*");
		appServlet.setLoadOnStartup(1);
		appServlet.setAsyncSupported(true);
	}

	private AnnotationConfigWebApplicationContext buildApplicationContext(String configLocation) {
		
		AnnotationConfigWebApplicationContext context;
		context = new AnnotationConfigWebApplicationContext();
		context.setConfigLocation(configLocation);
		
		return context;
	}

}

package br.com.project.botbuilder.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/gerenciador")
public class GerenciadorPartidasController {

	@ResponseBody
	@RequestMapping("/buscarPartida")
	public String buscarPartida() {
		return "{\"url\":\"/botbuilder/websocketgame\"}";
	}
}

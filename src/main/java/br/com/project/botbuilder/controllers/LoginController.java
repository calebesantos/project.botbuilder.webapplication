package br.com.project.botbuilder.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/login")
public class LoginController {
	
	@RequestMapping("/autenticarUsuario")
	public String autenticarUsuario() {
		System.out.println("autenticarUsuario()");
		return "redirect:/static/components/user/main.html";
	}
}

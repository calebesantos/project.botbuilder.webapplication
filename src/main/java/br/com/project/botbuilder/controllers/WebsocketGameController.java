package br.com.project.botbuilder.controllers;

import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.messaging.simp.SimpMessageHeaderAccessor;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.messaging.simp.annotation.SubscribeMapping;
import org.springframework.messaging.simp.stomp.StompHeaderAccessor;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.socket.messaging.SessionConnectedEvent;
import org.springframework.web.socket.messaging.SessionDisconnectEvent;

import br.com.project.botbuilder.websocket.ISessionHandler;
import br.com.project.botbuilder.websocket.Player;

@Controller
@RequestMapping("/websocketgame")
public class WebsocketGameController {

	@Autowired
	private SimpMessagingTemplate messageTemplate;
	
	private final ISessionHandler _sessionHandler;
	
	@Autowired
	public WebsocketGameController(ISessionHandler sessionHandler) {
		_sessionHandler = sessionHandler;
	}
	
	@RequestMapping(method = RequestMethod.GET)
	public String viewApplication() {
		return "forward:static/game/index.html";
	}
	
	@MessageMapping("/game")
	@SendTo("/topic/playermoved")
	public Player sendMessage(Player player) {
		System.out.println("playermoved");
		_sessionHandler.updatePositionPlayer(player);
		return player;
	}
	
	@SubscribeMapping("/playerid")
//	@SendTo("/topic/playerid")
	public Player sendPlayerId(SimpMessageHeaderAccessor headerAccessor) {
		String sessionId = headerAccessor.getSessionId();
		System.out.println("sendPlayerId"+sessionId);
		return _sessionHandler.getPlayerBySessionId(sessionId);
	}
	
	@SubscribeMapping("/playersconnected")
	public Set<Player> sendPlayersConnected(SimpMessageHeaderAccessor headerAccessor) {
		return _sessionHandler.getPlayersConnected(headerAccessor.getSessionId());
	}
	
	/*@EventListener
	public void getSessionConnect(SessionConnectEvent event) {
		
		StompHeaderAccessor sha = StompHeaderAccessor.wrap(event.getMessage());
		String sessionId = sha.getSessionId();
		
		Player p = _sessionHandler.createNewPlayer(sessionId);
		messageTemplate.convertAndSend("/topic/newplayer", p);
	}*/
	
	@EventListener
	public void getSessionConnected(SessionConnectedEvent event) {
		StompHeaderAccessor sha = StompHeaderAccessor.wrap(event.getMessage());
		
		String sessionId = sha.getSessionId();
		Player p = _sessionHandler.createNewPlayer(sessionId);
		messageTemplate.convertAndSend("/topic/newplayer", p);
	}
	
	@EventListener
	public void getSessionDisconnected(SessionDisconnectEvent event) {
		StompHeaderAccessor sha = StompHeaderAccessor.wrap(event.getMessage());
		String sessionId = sha.getSessionId();
		Player p = _sessionHandler.removePlayerBySessionId(sessionId);
		messageTemplate.convertAndSend("/topic/playerdisconnect", p);
	}
}

//private List<OutputMessage> messages;
/*

@MessageMapping("/chat")
	@SendTo("/topic/message")
	public OutputMessage sendMessage(Message message) {
		OutputMessage out = new OutputMessage(message, new Date());
		messages.add(out);
//		messages.stream().map(OutputMessage::getMessage).forEach(System.out::println);
		return out;
	}

@SubscribeMapping("/playersconnected")
public Set<Player> sendOldMessages() {
	return messages;
}

*/
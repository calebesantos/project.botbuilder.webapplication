package br.com.project.botbuilder.configuration;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import br.com.project.botbuilder.websocket.ISessionHandler;
import br.com.project.botbuilder.websocket.SessionHandler;

@Configuration
public class BeanConfiguration {

	@Bean
	public ISessionHandler getSessionHandler() {
		return new SessionHandler();
	}
}

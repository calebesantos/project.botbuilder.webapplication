package br.com.project.botbuilder.configuration;

import org.springframework.context.ApplicationListener;
import org.springframework.web.socket.messaging.SessionConnectEvent;

public class StompConnectEvent implements ApplicationListener<SessionConnectEvent> {

	@Override
	public void onApplicationEvent(SessionConnectEvent event) {
		System.out.println("Teste");
		System.out.println(event.getMessage());
	}

}

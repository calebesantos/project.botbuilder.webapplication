package br.com.project.botbuilder.websocket;

import java.util.HashSet;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class SessionHandler implements ISessionHandler {

	private long generateId;
	private Set<Player> players;

	public SessionHandler() {
		setPlayers(new HashSet<>());
	}

	@Override
	public Set<Player> getPlayersConnected() {
		return players;
	}

	@Override
	public void updatePositionPlayer(Player player) {

		Player playerSelected = getPlayerById(player.getId());

		if (playerSelected == null)
			return;

		playerSelected.setX(player.getX());
		playerSelected.setY(player.getY());
	}
	
	@Override
	public Player getPlayerById(long id) {
		
		Optional<Player> optional = players.stream().filter(p -> p.getId() == id).findFirst();
		
		if(optional.isPresent())
			return (Player) optional.get();
		
		return null;
	}

	public void setPlayers(Set<Player> players) {
		this.players = players;
	}

	@Override
	public Player createNewPlayer(String sessionId) {
		long id = ++generateId;
		
		Player p = new Player(id, 0, 0, sessionId);
		players.add(p);
		return p;
	}

	@Override
	public Player getPlayerBySessionId(String sessionId) {
		Optional<Player> optional;
		optional = players.stream().filter(p -> p.getSessionId().equals(sessionId)).findFirst();
		
		if(optional.isPresent())
			return (Player) optional.get();
		
		return null;
	}

	@Override
	public Player removePlayerBySessionId(String sessionId) {
		
		Player p = getPlayerBySessionId(sessionId);
		players.remove(p);
		
		return p;
	}

	@Override
	public Set<Player> getPlayersConnected(String sessionId) {
		
		Stream<Player> players = getPlayersConnected().stream();
		Stream<Player> listOtherPlayers = players.filter(p -> !p.getSessionId().equals(sessionId));
		
		return (Set<Player>) listOtherPlayers.collect(Collectors.toSet());
	}

}

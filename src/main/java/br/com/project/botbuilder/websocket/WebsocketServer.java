/*package br.com.project.botbuilder.websocket;

import java.io.IOException;
import java.util.Set;

import javax.websocket.CloseReason;
import javax.websocket.OnClose;
import javax.websocket.OnError;
import javax.websocket.OnMessage;
import javax.websocket.OnOpen;
import javax.websocket.Session;
import javax.websocket.server.ServerEndpoint;

import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

//@ServerEndpoint("/websocketgame")
public class WebsocketServer {

	private final ISessionHandler _sessionHandler;

	public WebsocketServer(ISessionHandler sessionHandler) {
		_sessionHandler = sessionHandler;
	}

	//@OnOpen
	public void onOpen(Session session) {

		ObjectMapper objectMapper = new ObjectMapper();
		String json;

		_sessionHandler.createNewPlayer(session);

		try {

			Set<Player> players = _sessionHandler.getPlayersConnected();

			json = objectMapper.writeValueAsString(new Retorno(players, "playersConnected"));

			_sessionHandler.sendToSession(session, json);

			Player player = _sessionHandler.getPlayerBySession(session);

			if (player == null)
				return;

			json = objectMapper.writeValueAsString(new Retorno(player, "newPlayer"));
			_sessionHandler.broadcast(session, json);

		} catch (IOException e) {
			System.out.println(e.getMessage());
		}
		
		 * JsonObject json = sessionHandler.getJsonPlayersConnected();
		 * 
		 * sessionHandler.sendToSession(session, json);
		 * 
		 * sessionHandler.addSession(session); Device device =
		 * sessionHandler.getDevice(session);
		 * 
		 * if(device == null) return;
		 * 
		 * String id = device.getId();
		 * 
		 * JsonProvider provider = JsonProvider.provider(); json =
		 * provider.createObjectBuilder() .add("id", id) .add("action",
		 * "socketId") .build();
		 * 
		 * sessionHandler.sendToSession(session, json);
		 * 
		 * json = provider.createObjectBuilder() .add("id", id) .add("action",
		 * "newPlayer") .build();
		 * 
		 * sessionHandler.sendToAllConnectedSessions(session, json);
		 
	}

	//@OnClose
	public void onClose(Session sesion, CloseReason closeReason) {
		
		 * Device device = sessionHandler.getDevice(session);
		 * 
		 * if(device == null) return;
		 * 
		 * String id = device.getId();
		 * 
		 * JsonProvider provider = JsonProvider.provider(); JsonObject json =
		 * provider.createObjectBuilder() .add("id", id) .add("action",
		 * "playerDisconnect") .build();
		 * 
		 * sessionHandler.removeSession(session);
		 * sessionHandler.sendToAllConnectedSessions(session, json);
		 
	}

	//@OnError
	public void onError(Throwable throwable) {

	}

	//@OnMessage
	public void onMessage(String message, Session session) {

		ObjectMapper objectMapper = new ObjectMapper();
		Retorno retorno;
		String jsonResult = "";
		try {
			retorno = objectMapper.readValue(message, Retorno.class);
			String action = retorno.getAction();

			switch (action) {
			case "playerMoved":

				Player player = (Player) retorno.getObject();
				_sessionHandler.updatePositionPlayer(session, player);
				break;

			default:
				break;
			}

			Player player = _sessionHandler.getPlayerBySession(session);

			jsonResult = objectMapper.writeValueAsString(new Retorno(player, "playerMoved"));
			_sessionHandler.broadcast(session, jsonResult);

		} catch (JsonGenerationException e) {
			e.printStackTrace();
		} catch (JsonMappingException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

		
		 * try (JsonReader reader = Json.createReader(new
		 * StringReader(message))) {
		 * 
		 * JsonObject jsonMessage = reader.readObject(); String action =
		 * jsonMessage.getString("action");
		 * 
		 * switch (action) { case "playerMoved":
		 * 
		 * Device device = sessionHandler.getDevice(session);
		 * 
		 * if (device == null) break;
		 * 
		 * device.setX(((Double)jsonMessage.getJsonNumber("x").doubleValue()).
		 * floatValue());
		 * device.setY(((Double)jsonMessage.getJsonNumber("y").doubleValue()).
		 * floatValue());
		 * 
		 * JsonProvider provider = JsonProvider.provider();
		 * 
		 * JsonObject json = provider.createObjectBuilder() .add("id",
		 * device.getId()) .add("x", device.getX()) .add("y", device.getY())
		 * .add("action", "playerMoved") .build();
		 * 
		 * sessionHandler.sendToAllConnectedSessions(session, json);
		 * 
		 * break; } }
		 
	}
}
*/
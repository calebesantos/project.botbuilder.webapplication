package br.com.project.botbuilder.websocket;

import javax.websocket.Session;;

public class PlayerSessionAggregate {

	private long id;
	private Player player;
	private Session session;

	public PlayerSessionAggregate() {
	}

	public PlayerSessionAggregate(long id, Player player, Session session) {
		this.id = id;
		this.player = player;
		this.session = session;
	}


	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public Player getPlayer() {
		return player;
	}

	public void setPlayer(Player player) {
		this.player = player;
	}

	public Session getSession() {
		return session;
	}

	public void setSession(Session session) {
		this.session = session;
	}

}

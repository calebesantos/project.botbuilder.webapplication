package br.com.project.botbuilder.websocket;

public class Retorno {

	private Object object;
	private String action;

	public Retorno() {
	}
	
	public Retorno(Object object, String action) {
		this.object = object;
		this.action = action;
	}
	
	public Object getObject() {
		return object;
	}

	public void setObject(Object object) {
		this.object = object;
	}

	public String getAction() {
		return action;
	}

	public void setAction(String action) {
		this.action = action;
	}
}

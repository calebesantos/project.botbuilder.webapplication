package br.com.project.botbuilder.websocket;

import java.util.Set;

public interface ISessionHandler {

	Set<Player> getPlayersConnected();
	
	Set<Player> getPlayersConnected(String sessionId);

	void updatePositionPlayer(Player player);

	void setPlayers(Set<Player> players);

	Player getPlayerById(long id);

	Player createNewPlayer(String sessionId);

	Player getPlayerBySessionId(String sessionId);

	Player removePlayerBySessionId(String sessionId);
}

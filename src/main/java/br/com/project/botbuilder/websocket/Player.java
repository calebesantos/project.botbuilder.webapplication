package br.com.project.botbuilder.websocket;

public class Player {

	private double x;
	private double y;
	private long id;
	private String sessionId;
	private int input;
	private int state;
	private boolean fliped;

	public Player(){}
	
	public Player(long id, double x, double y, String sessionId) {
		this.id = id;
		this.x = x;
		this.y = y;
		this.sessionId = sessionId;
	}

	public double getX() {
		return x;
	}

	public void setX(double x) {
		this.x = x;
	}

	public double getY() {
		return y;
	}

	public void setY(double y) {
		this.y = y;
	}

	public void setId(long id) {
		this.id = id;
	}

	public long getId() {
		return id;
	}
	
	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}

	public String getSessionId() {
		return sessionId;
	}

	public int getInput() {
		return input;
	}

	public void setInput(int input) {
		this.input = input;
	}

	public int getState() {
		return state;
	}

	public void setState(int state) {
		this.state = state;
	}

	public boolean getFliped() {
		return fliped;
	}

	public void setFliped(boolean fliped) {
		this.fliped = fliped;
	}
}
